const mockery = require('mockery');
const should = require('should');

describe('BeeQueue', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    mockery.registerMock('amqplib/callback_api', {
      connect: function (url, done) {
        return done(null, {
          createChannel: function (err, channel) {}
        });
      }
    });

    this.queue = require('../src/index');
  });

  after(function () {
    mockery.disable()
  });

  it('connect() should return a promise', function (done) {
    should(this.queue.connect()).be.Promise();
    should(this.queue.connect()).be.rejected();

    done();
  });

  it('getQueue() should return a promise', function (done) {
    const channel = { consume: function() { } };

    should(this.queue.getQueue(channel)).be.Promise();
    should(this.queue.getQueue(channel)).be.rejected();

    done();
  });

  it('addMessage() should return an undefined', function (done) {
    const channel = { sendToQueue: function () { } };
    const message = 'foo';

    should(this.queue.addMessage(channel, message)).be.undefined();

    done();
  });
});