const amqp = require('amqplib/callback_api');

const QUEUE_NAME = process.env.QUEUE_NAME || 'hello';
const QUEUE_URL = process.env.QUEUE_URL || 'amqp://queue';

module.exports.connect = () => {
  return new Promise((resolve, reject) => {
    amqp.connect(QUEUE_URL, function (err, conn) {
      if (err) return reject(err);

      conn.createChannel(function (err, channel) {
        if (err) return reject(err);

        channel.assertQueue(QUEUE_NAME, { durable: true });

        resolve(channel);
      });
    });
  });
};

module.exports.getQueue = function (channel) {
  return new Promise(
    function (resolve, reject) {
      channel.consume(QUEUE_NAME, function (msg) {
        resolve(msg);
      }, { noAck: true });
    }
  );
};

module.exports.addMessage = function(channel, message) {
  channel.sendToQueue(QUEUE_NAME, message, { persistent: true });
};