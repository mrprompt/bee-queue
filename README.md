# Bee-Queue

A simple RabbitMQ client.

## Install

```console
npm install --save bee-queue
```

## Usage

```javascript
const queue = require('bee-queue');

const consume = (channel) => {
  queue
    .getQueue(channel)
    .then((msg) => console.log(msg))
    .catch((err) => console.error(err));
};

queue
  .connect()
  .then((channel) => consume(channel))
  .catch(function (err) {
    console.error('Error:', err.message);
  });
```

## Testing

No tests yet :(
